import {TreeNode} from '../tree/treenode/node.model';
import {State} from '../tree/treenode/state.model';

export class Tree {
  static nodes: TreeNode[];
  static id: number;
  static state: State;

  constructor () {
    Tree.id = 0;
  }

  addRootNode (addedNode: TreeNode) {
    Tree.id ++;
    addedNode.id = Tree.id;
    Tree.nodes.push(addedNode);
  }

  addNode (addedNode: TreeNode) {
    Tree.state.ActiveNode.pushToChildren(addedNode);
  }

  removeNode () {
    if (Tree.state.ActiveNode.parent === null) {
      Tree.nodes.splice(Tree.nodes.findIndex(currentNode => (currentNode.id === Tree.state.ActiveNode.id)), 1);
    } else {
      // tslint:disable-next-line:max-line-length
      Tree.state.ActiveNode.children.splice(Tree.state.ActiveNode.children.findIndex(currentNode => (currentNode.id === Tree.state.ActiveNode.id)), 1);
    }
  }
}
