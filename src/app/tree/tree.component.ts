import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from './treenode/node.model';
import {State} from './treenode/state.model';
import {state} from '@angular/animations';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {

  static id = 0;
  subCategories: number;
  subItems: number;
  @Input() state: State = new State();


  treeNodes = <TreeNode[]>[];
  isFormInitiated = false;
  translog: string[] = [];
  public isEditable = false;

  constructor () {
    this.subCategories = 0;
    this.subItems = 0;
  }

  ngOnInit(): void {
  }

  addRootNode (addedNode: TreeNode) {
    addedNode.id = TreeComponent.id++;
    this.treeNodes.push(addedNode);
    this.translog.push('Added Category: ' + addedNode.categoryName);
    if (addedNode.typeCategory) {
      this.subCategories++;
    } else {
      this.subItems++;
    }
  }

  addNode (addedNode: TreeNode) {
    if (addedNode.typeCategory) {
      addedNode.id = TreeComponent.id++;
      addedNode.parent = this.state.ActiveNode;
      this.state.ActiveNode.pushToChildren(addedNode);
      this.translog.push('Added Sub-Category: ' + addedNode.categoryName + ' to ' + addedNode.parent.categoryName);
    } else {
      alert('Cannot Add Sub-Category/Item to an item');
    }
  }

  addNodeOnSidebarClick (addedNode: TreeNode) {
    addedNode.id = TreeComponent.id++;
    if (this.state.ActiveNode) {
      addedNode.parent = this.state.ActiveNode;
      this.state.ActiveNode.pushToChildren(addedNode);
      this.translog.push('Added Sub-Category: ' + addedNode.categoryName + ' to ' + addedNode.parent.categoryName);
    } else {
      this.treeNodes.push(addedNode);
      this.translog.push('Added Category: ' + addedNode.categoryName);
    }
  }

  removeNode () {
    this.translog.push('Removed Category: ' + this.state.ActiveNode.categoryName);
    if (this.state.ActiveNode.parent === null) {
      this.treeNodes.splice(this.treeNodes.findIndex(currentNode => (currentNode.id === this.state.ActiveNode.id)), 1);
    } else {
      // tslint:disable-next-line:max-line-length
      this.state.ActiveNode.parent.children.splice(this.state.ActiveNode.parent.children.findIndex(currentNode => (currentNode.id === this.state.ActiveNode.id)), 1);
    }
    this.resetActiveNode();
  }

  updateNode(nodeToBeChanged: TreeNode) {
    this.translog.push('Updated ' + this.state.ActiveNode.categoryName);
    this.state.ActiveNode.categoryName = nodeToBeChanged.categoryName;
    this.state.ActiveNode.categoryDescription = nodeToBeChanged.categoryDescription;
  }

  onActivation(event: TreeNode) {
    if (event === this.state.ActiveNode) {
      this.state.ActiveNode.isActive = false;
      this.state.ActiveNode = null;
    } else {
      if (this.state.ActiveNode) {
        this.state.ActiveNode.isActive = false;
      }
      this.state.ActiveNode = event;
      this.state.ActiveNode.isActive = true;
    }
    console.log(this.state);
  }

  resetActiveNode() {
    if (this.state.ActiveNode) {
      this.state.ActiveNode = null;
    }
  }

  changeEditable($event) {
    this.isEditable = !this.isEditable;
  }
}
