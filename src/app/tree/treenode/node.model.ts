export class TreeNode {


  public children: TreeNode[];
  public subcategories = 0;
  public subitems = 0;
  public isExpanding = false;
  // tslint:disable-next-line:max-line-length
  constructor(public id: number, public typeCategory: boolean, public categoryName: string, public categoryDescription: string, public parent: TreeNode, public isActive: boolean) {
    this.children = [];
    this.isActive = false;
  }

  // tslint:disable-next-line:max-line-length

  updateNode(node: TreeNode): boolean {
    try {
      if (node.categoryName === '' || node.categoryDescription === '') {
        this.categoryName = node.categoryName;
        this.categoryDescription = node.categoryDescription;
      } else {
        return false;
      }
      return true;
    } catch (e) {
      return false;
    }
  }

  pushToChildren(node: TreeNode) {
    this.children.push(node);
  }
}
