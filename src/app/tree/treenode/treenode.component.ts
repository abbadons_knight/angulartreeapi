import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TreeNode} from './node.model';

@Component({
  selector: 'app-treenode',
  templateUrl: './treenode.component.html',
  styleUrls: ['./treenode.component.css']
})
export class TreenodeComponent implements OnInit {

  @Input() treeNode: TreeNode;
  @Output() activateEmitter = new EventEmitter<TreeNode>();

  constructor() {
  }


  ngOnInit() {
  }

  nodeClicked() {
    console.log(this.treeNode.categoryName + ' Emitting event');
    this.activateEmitter.emit(this.treeNode);
  }

  dblClicked() {
    console.log(this.treeNode.children.length);
    this.treeNode.isExpanding = this.treeNode.isExpanding;
  }

  mouseOvering(event) {
    console.log(<HTMLInputElement>event.target);
  }

  nodeEmitted(childNode: TreeNode) {
    console.log(this.treeNode.categoryName + ' Passing event');
    this.activateEmitter.emit(childNode);
  }

  changeIsExpanding(event) {
    this.treeNode.isExpanding = !this.treeNode.isExpanding;
    console.log(this.treeNode.isExpanding);
  }
}
