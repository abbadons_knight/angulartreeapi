import {TreeNode} from './node.model';

export class State {
  activeNode: TreeNode;

  toggleActiveNode (node: TreeNode) {
    if (node.id === this.activeNode.id) {
      this.activeNode = null;
    } else {
      this.activeNode = node;
    }
  }

  get ActiveNode (): TreeNode {
    return this.activeNode;
  }
  set ActiveNode (activeNode: TreeNode) {
    this.activeNode = activeNode;
  }
}
