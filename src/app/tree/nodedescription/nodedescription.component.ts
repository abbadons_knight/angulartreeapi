import {Component, Input, OnInit} from '@angular/core';
import {TreeNode} from '../treenode/node.model';

@Component({
  selector: 'app-nodedescription',
  templateUrl: './nodedescription.component.html',
  styleUrls: ['./nodedescription.component.css']
})
export class NodedescriptionComponent implements OnInit {

  constructor() { }

  @Input() activatedNode: TreeNode;

  ngOnInit() {
  }

}
