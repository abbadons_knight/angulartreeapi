import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-transactionlog',
  templateUrl: './transactionlog.component.html',
  styleUrls: ['./transactionlog.component.css']
})
export class TransactionlogComponent implements OnInit {

  @Input() public translog = <string[]>[];
  public isOpen = false;

  constructor() {
    this.translog = [];
  }

  ngOnInit() {
  }

  toggleOpen(event) {
    this.isOpen = !this.isOpen;
  }
}
