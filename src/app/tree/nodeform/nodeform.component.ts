import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TreeNode} from '../treenode/node.model';
import {State} from '../treenode/state.model';

@Component({
  selector: 'app-nodeform',
  templateUrl: './nodeform.component.html',
  styleUrls: ['./nodeform.component.css']
})
export class NodeformComponent implements OnInit {

  @Input() state: State;

  @Output() addRootNode = new EventEmitter<TreeNode>();
  @Output() addNode = new EventEmitter<TreeNode>();
  @Output() deleteNode = new EventEmitter<TreeNode>();
  @Output() updateNode = new EventEmitter<TreeNode>();
  @Output() isFormInit = new EventEmitter<boolean>();

  public name: string;
  public desc: string;
  public typeOptions = ['Category', 'Item'];
  public selectedOption: string;

  constructor() { }

  ngOnInit() {
  }

  onAddRootClick() {
    if (this.selectedOption === 'Category') {
      this.addRootNode.emit(new TreeNode(0, true, this.name, this.desc, null, false));
    } else {
      this.addRootNode.emit(new TreeNode(0, false, this.name, this.desc, null, false));
    }
  }
  onAddClick() {
    if (this.selectedOption === 'Category') {
      this.addNode.emit(new TreeNode(0, true, this.name, this.desc, this.state.ActiveNode, false));
    } else {
      this.addNode.emit(new TreeNode(0, false, this.name, this.desc, this.state.ActiveNode, false));
    }
  }
  onDelClick() {
    this.deleteNode.emit(this.state.ActiveNode);
  }
  onUpdClick() {
    // tslint:disable:max-line-length
    this.updateNode.emit(new TreeNode(this.state.ActiveNode.id, this.state.ActiveNode.typeCategory, this.name, this.desc, this.state.ActiveNode.parent, this.state.ActiveNode.isActive));
  }
  onTextEdited() {
    if (name) {
      this.isFormInit.emit(true);
    } else {
      this.isFormInit.emit(false);
    }
  }
}
