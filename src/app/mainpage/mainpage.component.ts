import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TreeNode} from '../tree/treenode/node.model';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  @Output() addedNode = new EventEmitter<TreeNode>();
  @Output() deletedNode = new EventEmitter<TreeNode>();
  @Output() updatedNode = new EventEmitter<TreeNode>();
  constructor() { }

  ngOnInit() {
  }

  onAddNode(node: TreeNode) {
    this.addedNode.emit(node);
  }
  onDelNode(node: TreeNode) {
    this.deletedNode.emit(node);
  }
  onUpdNode(node: TreeNode) {
    this.updatedNode.emit(node);
  }
}
