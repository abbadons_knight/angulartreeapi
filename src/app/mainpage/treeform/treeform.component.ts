import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {TreeNode} from '../../tree/treenode/node.model';

@Component({
  selector: 'app-treeform',
  templateUrl: './treeform.component.html',
  styleUrls: ['./treeform.component.css']
})
export class TreeformComponent implements OnInit {

  activatedNode: TreeNode = null;

  @Output() addNode = new EventEmitter<TreeNode>();
  @Output() deleteNode = new EventEmitter<TreeNode>();
  @Output() updateNode = new EventEmitter<TreeNode>();

  @ViewChild('inputName') name: string;
  @ViewChild('inputDesc') desc: string;

  constructor() { }

  ngOnInit() {
  }

  onAddRootClick() {
    this.addNode.emit(new TreeNode(0, this.name, this.desc, null, false));
  }
  onAddClick() {
    this.addNode.emit(new TreeNode(0, this.name, this.desc, this.activatedNode, false));
  }
  onDelClick() {
    this.deleteNode.emit(this.activatedNode);
  }
  onUpdClick() {
    this.updateNode.emit(new TreeNode(this.activatedNode.id, this.name, this.desc, this.activatedNode.parent, this.activatedNode.isActive));
  }
}
