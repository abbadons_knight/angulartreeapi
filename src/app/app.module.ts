import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { TreeComponent } from './tree/tree.component';
import { TreenodeComponent } from './tree/treenode/treenode.component';
import { DescriptionComponent } from './description/description.component';
import { SidepanelComponent } from './sidepanel/sidepanel.component';
import { TreeformComponent } from './mainpage/treeform/treeform.component';
import { TreeNodeDirective } from './directives/tree-node.directive';
import { NodeformComponent } from './tree/nodeform/nodeform.component';
import { TransactionlogComponent } from './tree/transactionlog/transactionlog.component';
import { NodedescriptionComponent } from './tree/nodedescription/nodedescription.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainpageComponent,
    TreeComponent,
    TreenodeComponent,
    DescriptionComponent,
    SidepanelComponent,
    TreeformComponent,
    TreeNodeDirective,
    NodeformComponent,
    TransactionlogComponent,
    NodedescriptionComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
