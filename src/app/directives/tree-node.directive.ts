import {Directive, ElementRef, HostBinding, HostListener, Renderer2} from '@angular/core';

@Directive({
  selector: '[appTreeNode]'
})
export class TreeNodeDirective {

  @HostBinding('style.backgroundColor') backColor = 'transparent;';

  constructor(private elementReference: ElementRef, private renderer: Renderer2) { }

  @HostListener('onmouseenter') mouseentered(eventData: Event) {
    this.backColor = 'azure';
  }

  @HostListener('onmouseleave') mouseleft(eventData: Event) {
    this.backColor = 'transparent';
  }
}
